# ELCIP (Explorer Log Ciputra)
![](./src-readme/elcip-1-1.png)  

A native iOS App for member of Apple Developer Academy (ADA) to get to know other each other better
by giving common Individual information, experience, and Flash Card Game.

## Background
The usual way to get information of member Apple Developer Academy is to open third party web application. I found that people are reluctant to open that web application and prefer iOS apps. Also, many people find it difficult to memorize, and usually as an aid to memorization, flash card games are used to help memorize.

## How to Run
1. start a configured [ExplorerLOG API backend](https://github.com/davindj/explorer-log-backend)
2. clone / download this project
3. open `Explorer Log.xcodeproj` in [xcode](https://developer.apple.com/xcode/)
4. setup client app's server url in Edit Scheme > Run > Environment Variables > `SERVER_URL`
5. build & run project

_notes: if you want the app to run without using xcode, you need to change BASE\_URL output in `Explorer Log/api.swift`_
```swift
...
struct API{
    static var BASE_URL: String{
        "http://192.168.100.17:3000" // <-- EXAMPLE
    }
...
```