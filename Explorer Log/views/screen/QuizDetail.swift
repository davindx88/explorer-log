//
//  QuizDetail.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

struct QuizDetail: View {
    @State var explorers: [Explorer] = []
    @State var options: [String] = []
    @State var answerExplorer: Explorer? = nil
    @State var score: Int = 0
    // Alert
    @State var alertDesc: String = ""
    @State var alertTitle: String = ""
    @State var alertIsActive: Bool = false
    
    var body: some View {
        ZStack{
            VStack{
                Text("Score: \(score)")
                    .bold()
                    .font(.title3)
                    .foregroundColor(.orange)
                Spacer()
            }.ignoresSafeArea()
            VStack{
                Spacer()
                Text("Who is this?")
                    .font(.largeTitle)
                    .bold()
                    .foregroundColor(.orange)
                if let answer = self.answerExplorer{
                    RemoteImage(url: answer.imageURL)
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 160, height: 160, alignment: .center)
                        .clipped()
                        .cornerRadius(10)
                }
                ForEach((0..<options.count), id: \.self){ idx in
                    Button(options[idx]){
                        checkAnswer(option: options[idx])
                    }
                    .foregroundColor(.orange)
                    .padding()
                    .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    .cornerRadius(5)
                }
                Spacer()
                Spacer()
            }
        }
        .alert(isPresented: $alertIsActive){
            Alert(title: Text(alertTitle),
                  message: Text(alertDesc),
                  dismissButton: .default(Text("Continue")){
                    generateQuestion()
                  })
        }
        .navigationBarTitle("Score: \(score)", displayMode: .inline)
        .onAppear{
            loadData()
        }
    }
    func loadData(){
        API.getAllExplorers{ explorers in
            self.explorers = explorers
            generateQuestion()
        }
    }
    
    func checkAnswer(option: String){
        // Check Jawaban User
        print("Choosen Option: \(option)")
        let isCorrect = answerExplorer!.name == option
        if isCorrect{
            score += 10
        }
        // Update Alert
        alertTitle = isCorrect ? "Correct" : "Wrong"
        alertDesc = "Answer: \(answerExplorer!.name)"
        alertIsActive = true
        // Clear
        answerExplorer = nil
        options = []
    }
    
    func generateQuestion(){
        print("Generating Question")
        // Random
        explorers.shuffle()
        answerExplorer = explorers[0]
        // Generate Option
        options = explorers[0..<3].map{ $0.name }.shuffled()
    }
}

struct QuizDetail_Previews: PreviewProvider {
    static var previews: some View {
        QuizDetail()
    }
}
