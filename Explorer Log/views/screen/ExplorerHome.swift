//
//  ExplorerHome.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 04/05/21.
//

import SwiftUI

struct ExplorerHome: View {
    @State var seniorLearners: [Explorer] = []
    @State var juniorLearners: [Explorer] = []
    
    var body: some View {
        ZStack{
            Color(red: 232, green: 232, blue: 232)
                .ignoresSafeArea()
            ScrollView(showsIndicators: false){
                VStack(spacing: 20){
                    VStack(alignment: .leading, spacing: 5){
                        HStack(alignment: .bottom){
                            Text("Senior Learner")
                                .font(.title)
                                .fontWeight(.bold)
                            Spacer()
                            NavigationLink(destination: ExplorerList(title: "Senior Learner",
                                                                     explorers: seniorLearners)){
                                Text("See All")
                                    .font(.body)
                                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                                    .foregroundColor(.orange)
                            }
                        }.padding(.horizontal)
                        ScrollView(.horizontal, showsIndicators: false){
                            LazyHStack(alignment: .top){
                                ForEach(seniorLearners){ seniorLearner in
                                    ExplorerCard(explorer: seniorLearner)
                                }
                            }.padding(.horizontal)
                        }
                    }
                    VStack{
                        HStack(alignment: .bottom){
                            Text("Junior Learner")
                                .font(.title)
                                .fontWeight(.bold)
                            Spacer()
                            NavigationLink(destination: ExplorerList(title: "Junior Learner",
                                                                     explorers: juniorLearners)){
                                Text("See All")
                                    .font(.body)
                                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                                    .foregroundColor(.orange)
                            }
                        }.padding(.horizontal)
                        ScrollView(.horizontal, showsIndicators: false){
                            LazyHStack(alignment: .top){
                                ForEach(juniorLearners){ juniorLearner in
                                    ExplorerCard(explorer: juniorLearner)
                                }
                            }.padding(.horizontal)
                        }
                    }
                    Spacer()
                }
            }
        }
        .navigationBarTitle("Explorer")
        .onAppear(perform: loadData)
    }
    
    func loadData(){
        API.getAllSeniorExplorers{ explorers in
            self.seniorLearners = explorers
        }
        API.getAllJuniorExplorers{ explorers in
            self.juniorLearners = explorers
        }
    }
}

struct ExplorerHomeView_Previews: PreviewProvider {
    static var previews: some View {
        ExplorerHome()
    }
}
