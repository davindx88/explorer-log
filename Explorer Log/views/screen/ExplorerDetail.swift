//
//  ExplorerDetail.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct ExplorerDetail: View {
    @ObservedObject var explorer: Explorer
    @State var teams: [Team]? = []
    @State var mentor: Explorer? = nil
    
    var body: some View {
        ZStack{
            Color(red: 232, green: 232, blue: 232)
                .ignoresSafeArea()
            ScrollView{
                VStack(spacing: 0){
                    ProfileView(explorer: explorer)
                    if let teams = self.teams, !teams.isEmpty {
                        ExplorerTeamView(teams: teams)
                    }
                    if let mentor = self.mentor{
                        ExplorerMentorView(mentor: mentor)
                    }
                }
            }
        }
        .navigationBarTitle(explorer.name, displayMode: .inline)
        .onAppear{ loadData() }
    }
    
    func loadData(){
        API.getAllTeamsByMember(memberName: explorer.name){ teams in
            self.teams = teams
        }
        if let mentorName = explorer.mentor{
            API.getExplorer(name: mentorName){ explorer in
                self.mentor = explorer
            }
        }
    }
}

// MARK: Profile View
struct ProfileView: View{
    var explorer: Explorer
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                RemoteImage(url: explorer.imageURL)
                    .aspectRatio(contentMode: .fit)
                    .clipped()
                Text(explorer.name)
                    .font(.title)
                    .bold()
                Text(explorer.isMentor ? "Senior Learner" : "Junior Learner")
                    .font(.title2)
                    .italic()
                    .padding(.bottom, 10)
                Text(explorer.expertise.text)
                    .font(.body)
                    .padding(.vertical, 4)
                    .padding(.horizontal, 16)
                    .background(explorer.expertise.color)
                    .cornerRadius(20)
                Text(explorer.shift.text)
                    .font(.body)
                    .padding(.vertical, 4)
                    .padding(.horizontal, 16)
                    .background(explorer.shift.color)
                    .cornerRadius(20)
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

// MARK: Team View
struct ExplorerTeamView: View{
    var teams: [Team]
    
    var body: some View{
        VStack(spacing: 5){
            HStack{
                Text("Teams")
                    .font(.title)
                    .bold()
                Spacer()
            }.padding(.horizontal)
            ScrollView(.horizontal){
                HStack{
                    ForEach((0..<teams.count), id: \.self){ idx in
                        TeamCard(team: teams[idx])
                    }
                }.padding(.horizontal)
            }
        }
        .padding(.vertical)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

// MARK: Mentor View
struct ExplorerMentorView: View{
    var mentor: Explorer
    
    var body: some View{
        HStack{
            VStack(spacing: 5){
                HStack{
                    Text("LQ Mentor")
                        .font(.title)
                        .bold()
                    Spacer()
                }
                ExplorerCard(explorer: mentor)
            }
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct ExplorerDetail_Previews: PreviewProvider {
    static var explorer1: Explorer{
        Explorer( name: "Davin Djayadi",
                  expertise: .tech,
                  shift: .morning,
                  image: "DavinDjayadi",
                  mentor: "Gabriele Wijasa",
                  isMentor: false)
    }
    static var explorer2: Explorer{
        Explorer( name: "Gabriele Wijasa",
                  expertise: .design,
                  shift: .all,
                  image: "GabrieleWijasa",
                  mentor: nil,
                  isMentor: true)
    }
    
    static var previews: some View {
        ExplorerDetail(explorer: ExplorerDetail_Previews.explorer1)
        VStack{
            ProfileView(explorer: ExplorerDetail_Previews.explorer1)
            ProfileView(explorer: ExplorerDetail_Previews.explorer1)
        }
    }
}
