//
//  TeamHome.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import SwiftUI

struct TeamHome: View {
    @State var teams: [Team] = []
    
    var body: some View {
        ZStack{
            Color(red: 232, green: 232, blue: 232)
                .ignoresSafeArea()
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                    LazyVStack{
                        ForEach((0..<teams.count), id: \.self){ idx in
                            TeamCard(team: teams[idx])
                        }
                    }.padding(.horizontal)
                }
            }
        }
        .navigationBarTitle("Team")
        .onAppear(perform: loadData)
    }
    
    func loadData(){
        API.getAllTeams{ teams in
            self.teams = teams
        }
    }
}

struct TeamHome_Previews: PreviewProvider {
    static var previews: some View {
        TeamHome()
    }
}
