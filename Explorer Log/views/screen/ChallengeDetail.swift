//
//  ChallengeDetail.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

enum Loader{
    case idle
    case loading
    case loaded
}

struct ChallengeDetail: View {
    var challenge: Challenge
        
    @State var teams: [Team] = []
    @State var loader: Loader = .idle
    
    var body: some View {
        ScrollView{
            VStack(spacing: 0){
                ChallengeTitleView(name: challenge.name, dateText: challenge.dateText)
                ChallengeTypeView(isTeam: challenge.isTeam)
                ChallengeTeamsView(loader: $loader, teams: teams)
            }
        }
        .navigationBarTitle(challenge.name, displayMode: .inline)
        .onAppear{ loadData() }
    }
    
    func loadData(){
        loader = .loading
        API.getAllTeamsByChallengeId(challengeId: challenge.id){ teams in
            self.teams = teams
            loader = .loaded
        }
    }
}

struct ChallengeTitleView: View{
    var name: String
    var dateText: String
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text(name)
                    .font(.title)
                    .bold()
                Text(dateText)
                    .font(.title2)
                    .foregroundColor(.secondary)
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct ChallengeTypeView: View{
    var isTeam: Bool
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text("Type")
                    .font(.title3)
                    .foregroundColor(.secondary)
                Label(isTeam ? "Team" : "Individual",
                      systemImage: isTeam ? "person.3.fill" : "person.fill")
                    .font(.title)
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct ChallengeTeamsView: View{
    @Binding var loader: Loader
    var teams: [Team]
    
    var body: some View{
        VStack(spacing: 5){
            HStack{
                Text("Teams")
                    .font(.title)
                    .bold()
                Spacer()
            }.padding(.horizontal)
            if loader == .loading{
                Text("Loading teams...")
                    .padding(20)
            }else if loader == .loaded{
                if teams.isEmpty{
                    Text("No team found")
                        .padding(20)
                }else{
                    ScrollView(.horizontal){
                        LazyVStack{
                            ForEach((0..<3), id: \.self){ i in
                                ScrollView(.horizontal){
                                    LazyHStack{
                                        ForEach((0...(teams.count-1)/3+1), id: \.self){ j in
                                            if i*(teams.count-1)/3+j < teams.count{
                                                TeamCard(team: teams[i*(teams.count-1)/3+j])
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }.padding(.vertical)
                }
            }
            
        }
        .padding(.vertical)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct ChallengeDetail_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeDetail(challenge: Challenge(id:"0",
                                             type: .nanoChallenge,
                                             idx: 2,
                                             startDate: Date(),
                                             endDate: Date(),
                                             isTeam: false))
    }
}
