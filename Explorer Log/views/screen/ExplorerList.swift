//
//  ExplorerList.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

struct ExplorerList: View {
    var title: String
    var explorers: [Explorer]

    var body: some View {
        ScrollView{
            LazyVStack{
                ForEach((0..<(explorers.count+1)/2), id: \.self){ i in
                    HStack{
                        ForEach((0...1), id: \.self){ j in
                            if i*2+j < explorers.count{
                                ExplorerCard(explorer: explorers[i*2+j])
                            }
                            
                        }
                    }
                }
            }
        }.navigationBarTitle(title)
    }
}

struct ExplorerList_Previews: PreviewProvider {
    static var previews: some View {
        ExplorerList(title: "Junior Learner", explorers: [
            Explorer(name: "Davin Djayadi",
                     expertise: .tech,
                     shift: .morning,
                     image: "DavinDjayadi",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin Djayadi asdas da",
                     expertise: .design,
                     shift: .afternoon,
                     image: "DavinDjayadi",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin Djayadi",
                     expertise: .domainExpert,
                     shift: .all,
                     image: "Gabriele",
                     mentor: nil,
                     isMentor: true)
        ])
    }
}
