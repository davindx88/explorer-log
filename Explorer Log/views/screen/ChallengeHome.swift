//
//  ChallengeHome.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import SwiftUI

struct ChallengeHome: View {
    @State var challenges: [Challenge] = []
    
    var body: some View {
        ScrollView{
            VStack{
                ForEach((0..<challenges.count),id: \.self){ idx in
                    ChallengeCard(challenge: challenges[idx])
                }
            }
        }
        .navigationBarTitle("Challenge")
        .onAppear(perform: loadData)
    }
    
    func loadData() {
        API.getAllChallenges{ challenges in
            self.challenges = challenges
        }
    }
}

struct ChallengeHome_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeHome()
    }
}
