//
//  TeamDetail.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import SwiftUI

struct TeamDetail: View {
    @State var team: Team
    @State var mentor: Explorer? = nil
    
    var body: some View {
        ZStack{
            Color(red: 232, green: 232, blue: 232)
                .ignoresSafeArea()
            ScrollView{
                VStack(spacing: 0){
                    TeamTitleView(name: team.name, shift: team.shift)
                    TeamChallengeView(challenge: team.challenge)
                    TeamIdeaView(idea: team.idea)
                    TeamStatementView(statement: team.challengeStatement)
                    TeamMemberView(members: team.members)
                    TeamDetailMentorView(mentor: team.mentor)
                }
            }
        }
        .navigationBarTitle(team.name, displayMode: .inline)
    }
}

struct TeamTitleView: View{
    var name: String
    var shift: Shift
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text(name)
                    .font(.title)
                    .bold()
                Text(shift.text)
                    .font(.body)
                    .padding(.vertical, 4)
                    .padding(.horizontal, 16)
                    .background(shift.color)
                    .cornerRadius(20)
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}



struct TeamChallengeView: View{
    var challenge: Challenge
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text("Challenge")
                    .font(.title)
                    .bold()
                ChallengeCard(challenge: challenge)
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct TeamIdeaView: View{
    var idea: String
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text("Big Idea")
                    .font(.title3)
                    .foregroundColor(.secondary)
                Text(idea)
                    .font(.title)
                    .bold()
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct TeamStatementView: View{
    var statement: String
    
    var body: some View{
        HStack{
            VStack(alignment: .leading, spacing: 5){
                Text("Challenge Statement")
                    .font(.title3)
                    .foregroundColor(.secondary)
                Text(statement.isEmpty ? "-" : statement)
                    .font(.title3)
                    .bold()
            }
            Spacer()
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct TeamMemberView: View{
    var members: [Explorer]
    
    var body: some View{
        VStack(spacing: 5){
            HStack{
                Text("Members")
                    .font(.title)
                    .bold()
                Spacer()
            }.padding(.horizontal)
            .padding(.horizontal,10)
            ScrollView(.horizontal, showsIndicators: false){
                HStack(alignment: .top){
                    ForEach((0..<members.count), id: \.self){ idx in
                        ExplorerCard(explorer: members[idx])
                    }
                }.padding(.horizontal)
            }
        }
        .padding(.vertical)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}

struct TeamDetailMentorView: View{
    var mentor: Explorer
    
    var body: some View{
        HStack{
            VStack(spacing: 5){
                HStack{
                    Text("Mentor")
                        .font(.title)
                        .bold()
                    Spacer()
                }
                ExplorerCard(explorer: mentor)
            }
        }
        .padding()
        .padding(.horizontal, 10)
        .padding(.vertical, 15)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.bottom, 8)
    }
}


struct TeamDetail_Previews: PreviewProvider {
    static var previews: some View {
        TeamDetail(team: Team(members: [
            Explorer(name: "Davin",
                     expertise: .tech,
                     shift: .afternoon,
                     image: "https://dl.airtable.com/.attachmentThumbnails/b0e63c253f7d1b62aa145f203eb009c1/2f49bd57",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin",
                     expertise: .tech,
                     shift: .afternoon,
                     image: "VincentiusPhillips",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin",
                     expertise: .tech,
                     shift: .afternoon,
                     image: "SadrakhSetyo",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin",
                     expertise: .tech,
                     shift: .afternoon,
                     image: "GeraldaCarnelian",
                     mentor: "Gabriele Wijasa",
                     isMentor: false),
            Explorer(name: "Davin",
                     expertise: .tech,
                     shift: .afternoon,
                     image: "Gabriele",
                     mentor: nil,
                     isMentor: true)
        ],
        name: "Team 1 / Techy Techy",
        shift: .afternoon,
        idea: "Technology",
        challengeStatement: "Challenge Statement...",
        mentor: Explorer(name: "Davin Djayadi",
                         expertise: .domainExpert,
                         shift: .all,
                         image: "Gabriele",
                         mentor: nil,
                         isMentor: true),
        app: "DUIX",
        challenge: Challenge(id:"0",
                             type: .macroChallenge,
                             idx: 2,
                             startDate: Date(),
                             endDate: Date(),
                             isTeam: true)
        ))
    }
}
