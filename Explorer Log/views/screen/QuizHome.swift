//
//  QuizHome.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

struct QuizHome: View {
    var body: some View {
        ZStack{
            Color.orange
                .ignoresSafeArea()
            VStack(alignment: .center){
                Spacer()
                Spacer()
                Text("Test your Apple Developer Academy social network?")
                    .font(.largeTitle)
                    .bold()
                    .foregroundColor(.white)
                Spacer()
                NavigationLink(
                    destination: QuizDetail()
                ){
                    Text("Play")
                        .font(.title3)
                        .bold()
                        .foregroundColor(.orange)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(5)
                }
                Spacer()
                Spacer()
            }
        }
    }
}

struct QuizHome_Previews: PreviewProvider {
    static var previews: some View {
        QuizHome()
    }
}
