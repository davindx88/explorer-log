//
//  TeamNavigation.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct TeamNavigation: View {
    var body: some View {
        NavigationView{
            TeamHome()            
        }
    }
}

struct TeamNavigation_Previews: PreviewProvider {
    static var previews: some View {
        TeamNavigation()
    }
}
