//
//  ChallengeNavigation.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct ChallengeNavigation: View {
    var body: some View {
        NavigationView{
            ChallengeHome()
        }
    }
}

struct ChallengeNavigation_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeNavigation()
    }
}
