//
//  QuizNavigation.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

struct QuizNavigation: View {
    var body: some View {
        NavigationView{
            QuizHome()
        }
    }
}

struct QuizNavigation_Previews: PreviewProvider {
    static var previews: some View {
        QuizNavigation()
    }
}
