//
//  MainNavigation.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct MainNavigation: View {
    var body: some View {
        TabView{
            ExplorerNavigation()
                .tabItem {
                    Label("Explorer", systemImage: "person.fill")
                }
            TeamNavigation()
                .tabItem {
                    Label("Team", systemImage: "person.3.fill")
                }
            ChallengeNavigation()
                .tabItem {
                    Label("Challenge", systemImage: "flag.fill")
                }
            QuizNavigation()
                .tabItem {
                    Label("Game", systemImage: "questionmark.square.fill")
                }
        }.accentColor(.orange)
    }
}

struct MainNavigation_Previews: PreviewProvider {
    static var previews: some View {
        MainNavigation()
    }
}
