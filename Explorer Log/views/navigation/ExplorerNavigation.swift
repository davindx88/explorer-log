//
//  ExplorerNavigation.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct ExplorerNavigation: View {
    var body: some View {
        NavigationView{
            ExplorerHome()
        }
    }
}

struct ExplorerNavigation_Previews: PreviewProvider {
    static var previews: some View {
        ExplorerNavigation()
    }
}
