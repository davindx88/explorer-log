//
//  ChallengeCard.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import SwiftUI

struct ChallengeCard: View {
    // Var
    @ObservedObject var challenge: Challenge
    @State var isNavigate: Bool = false
        
    var body: some View {
        NavigationLink(destination: ChallengeDetail(challenge: self.challenge),
                       isActive: $isNavigate){
             EmptyView()
        }
        VStack(alignment: .leading){
            HStack(spacing: 0){
                Text(challenge.type.text)
                    .font(.title2)
                    .bold()
                    .foregroundColor(challenge.type.colorDark)
                Text(" challenge \(challenge.idx)")
                    .font(.title2)
                    .bold()
            }
            HStack{
                Text(challenge.dateText)
                    .foregroundColor(.secondary)
                Spacer()
                Label(challenge.isTeam ? "Team" : "Individual",
                      systemImage: challenge.isTeam ? "person.3.fill" : "person.fill")
            }
            
        }
        .padding()
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
        .padding(5)
        .padding(.horizontal, 3)
        .padding(.bottom, 8)
        .onTapGesture {
            self.isNavigate = true
        }
    }
}

struct ChallengeCard_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeCard(challenge: Challenge(id: "0",
                                           type: .nanoChallenge,
                                           idx: 2,
                                           startDate: Date(),
                                           endDate: Date(),
                                           isTeam: false))
    }
}
