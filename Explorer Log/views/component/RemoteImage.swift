//
//  RemoteImage.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 07/05/21.
//

import SwiftUI

struct RemoteImage: View {
    private enum LoadState {
        case loading, success, failure
    }

    private class Loader: ObservableObject {
        var image: UIImage = UIImage(systemName: "photo")!
        var state = LoadState.loading

        init(url: String) {
            guard let parsedURL = URL(string: url) else {
                fatalError("Invalid URL: \(url)")
            }
            
            let keyCache = url as NSString
            if Cache.isImageCached(key: keyCache){
                self.image = Cache.getImageCache(key: keyCache)
                self.state = .success
            }
            
            let request = URLRequest(url: parsedURL)
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data, data.count > 0 {
                    if let image = UIImage(data: data) {
                        Cache.setImageCache(key: keyCache, image: image)
                        self.image = image
                        self.state = .success
                    }else{
                        self.state = .failure
                    }
                } else {
                    self.state = .failure
                }

                DispatchQueue.main.async {
                    self.objectWillChange.send()
                }
            }.resume()
        }
    }

    @StateObject private var loader: Loader
    var loading: Image
    var failure: Image

    var body: some View {
        selectImage()
            .resizable()
    }

    init(url: String, loading: Image = Image(systemName: "photo"), failure: Image = Image(systemName: "multiply.circle")) {
        _loader = StateObject(wrappedValue: Loader(url: url))
        self.loading = loading
        self.failure = failure
    }

    private func selectImage() -> Image {
        switch loader.state {
        case .loading:
            return loading
        case .failure:
            return failure
        case .success:
            return Image(uiImage: loader.image)
        }
    }
}

struct RemoteImage_Previews: PreviewProvider {
    static var previews: some View {
        RemoteImage(url: "https://cdn.searchenginejournal.com/wp-content/uploads/2020/02/9815e23f-c21c-4e9c-a18c-c5ce2c7dc39f-5e5b3c2bb5cba-1520x800.jpeg")
    }
}
