//
//  TeamCard.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import SwiftUI

struct TeamCard: View {
    // Prop
    var team: Team
    // State
    @State var isNavigate: Bool = false
    
    var body: some View {
        NavigationLink(destination: TeamDetail(team: team),
                       isActive: $isNavigate){
            HStack{
                VStack(alignment: .leading, spacing: 8){
                    VStack(alignment: .leading){
                        Text(team.name)
                            .font(.title3)
                            .bold()
                        Text(team.idea)
                            .font(.body)
                        HStack{
                            Text(team.challenge.name)
                                .font(.caption)
                                .padding(2)
                                .padding(.horizontal, 8)
                                .foregroundColor(.black)
                                .background(team.challenge.type.color)
                                .cornerRadius(20)
                            Spacer()
                            Text(team.shift.text)
                                .font(.caption)
                                .padding(2)
                                .padding(.horizontal, 8)
                                .foregroundColor(.black)
                                .background(team.shift.color)
                                .cornerRadius(20)
                        }
                    }
                    .padding(.horizontal)
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack{
                            ForEach(0..<team.members.count, id: \.self){ idx in
                                RemoteImage(url: team.members[idx].imageURL)
                                    .aspectRatio(contentMode: .fit)
                                    .scaleEffect(2, anchor: .top)
                                    .frame(width: 64, height: 64, alignment: .top)
                                    .cornerRadius(100)
                                    .clipped()
                            }
                        }
                        .padding(.horizontal)
                    }
                }
            }
            .padding(.vertical)
            .frame(width: 320)
            .background(Color.white)
            .cornerRadius(10)
            .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
            .padding(5)
            .padding(.bottom, 8)
            .onTapGesture {
                self.isNavigate = true
            }
        }.buttonStyle(.plain)
    }
}

struct TeamCard_Previews: PreviewProvider {
    static var previews: some View {
        TeamCard(team: Team(members: [
                                Explorer(name: "Davin",
                                         expertise: .tech,
                                         shift: .afternoon,
                                         image: "https://dl.airtable.com/.attachmentThumbnails/b0e63c253f7d1b62aa145f203eb009c1/2f49bd57",
                                         mentor: "Gabriele Wijasa",
                                         isMentor: false),
                                Explorer(name: "Davin",
                                         expertise: .tech,
                                         shift: .afternoon,
                                         image: "VincentiusPhillips",
                                         mentor: "Gabriele Wijasa",
                                         isMentor: false),
                                Explorer(name: "Davin",
                                         expertise: .tech,
                                         shift: .afternoon,
                                         image: "SadrakhSetyo",
                                         mentor: "Gabriele Wijasa",
                                         isMentor: false),
                                Explorer(name: "Davin",
                                         expertise: .tech,
                                         shift: .afternoon,
                                         image: "GeraldaCarnelian",
                                         mentor: "Gabriele Wijasa",
                                         isMentor: false),
                                Explorer(name: "Davin",
                                         expertise: .tech,
                                         shift: .afternoon,
                                         image: "Gabriele",
                                         mentor: nil,
                                         isMentor: true)
                            ],
                            name: "Team 1 / Techy Techy",
                            shift: .afternoon,
                            idea: "Technology",
                            challengeStatement: "Challenge Statement...",
                            mentor: Explorer(name: "Davin Djayadi",
                                             expertise: .domainExpert,
                                             shift: .all,
                                             image: "Gabriele",
                                             mentor: nil,
                                             isMentor: true),
                            app: "DUIX",
                            challenge: Challenge(id:"0",
                                                 type: .macroChallenge,
                                                 idx: 2,
                                                 startDate: Date(),
                                                 endDate: Date(),
                                                 isTeam: true)
                            ))
    }
}
