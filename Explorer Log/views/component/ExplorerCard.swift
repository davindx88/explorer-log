//
//  ExplorerCard.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import SwiftUI

struct ExplorerCard: View {
    // Prop
    var explorer: Explorer
    // State
    @State var isNavigate: Bool = false
    
    var body: some View {
        NavigationLink(destination: ExplorerDetail(explorer: explorer),
                       isActive: $isNavigate){
            VStack(spacing: 0){
                ExplorerHeaderView(imageURL: explorer.imageURL)
                ExplorerBodyView(name: explorer.name,
                                 expertise: explorer.expertise,
                                 shift: explorer.shift)
            }
            .frame(width: 160)
            .background(Color.white)
            .cornerRadius(10)
            .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)), radius: 4, x: 0.0, y: 2.0)
            .padding(5)
            .padding(.horizontal, 3)
            .padding(.bottom, 8)
            .onTapGesture {
                self.isNavigate = true
            }
        }
        .buttonStyle(.plain)
    }
}

struct ExplorerHeaderView: View{
    var imageURL: String
    var body: some View{
        RemoteImage(url: imageURL)
            .aspectRatio(contentMode: .fit)
            .scaleEffect(2, anchor: .top)
            .frame(width: 160, height: 160, alignment: .top)
            .clipped()
    }
}

struct ExplorerBodyView: View {
    var name: String
    var expertise: Expertise
    var shift: Shift
    
    var body: some View{
        HStack{
            VStack(alignment: .leading){
                Text(name)
                    .font(.body)
                    .bold()
                    .lineLimit(2)
                Text(expertise.text)
                    .font(.caption)
                    .padding(2)
                    .padding(.horizontal, 8)
                    .foregroundColor(.black)
                    .background(expertise.color)
                    .cornerRadius(20)
                Text(shift.text)
                    .font(.caption)
                    .padding(2)
                    .padding(.horizontal, 8)
                    .foregroundColor(.black)
                    .background(shift.color)
                    .cornerRadius(20)
            }.padding(.leading, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
            Spacer()
        }.padding(.vertical, 16)
    }
}

struct ExplorerCard_Previews: PreviewProvider {
    static var previews: some View {
        HStack(alignment: .top){
            ExplorerCard(explorer: Explorer(name: "Davin Djayadi",
                                            expertise: .tech,
                                            shift: .morning,
                                            image: "DavinDjayadi",
                                            mentor: "Gabriele Wijasa",
                                            isMentor: false))
            ExplorerCard(explorer: Explorer(name: "Davin Djayadi asdas da",
                                            expertise: .design,
                                            shift: .afternoon,
                                            image: "DavinDjayadi",
                                            mentor: "Gabriele Wijasa",
                                            isMentor: false))
            ExplorerCard(explorer: Explorer(name: "Davin Djayadi",
                                            expertise: .domainExpert,
                                            shift: .all,
                                            image: "Gabriele",
                                            mentor: nil,
                                            isMentor: true))
        }
    }
}
