//
//  Challenge.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import Foundation
import SwiftUI

class Challenge: ObservableObject, Codable{
    // Base Property
    @Published var id: String
    @Published var type: ChallengeType
    @Published var idx: Int
    @Published var startDate: Date
    @Published var endDate: Date
    @Published var isTeam: Bool
    
    // Computed Property
    var name: String{
        "\(type.text) Challenge \(idx)"
    }
    var dateText: String{
        let df = DateFormatter()
        df.dateFormat = "dd MMM"
        let sDate = df.string(from: startDate)
        let eDate = df.string(from: endDate)
        
        return "\(sDate) - \(eDate)"
    }
    
    // MARK: Codable
    enum CodingKeys: CodingKey {
        case id, type, idx, startDate, endDate, isTeam
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(type, forKey: .id)
        try container.encode(type, forKey: .type)
        try container.encode(idx, forKey: .idx)
        try container.encode(startDate, forKey: .startDate)
        try container.encode(endDate, forKey: .endDate)
        try container.encode(isTeam, forKey: .isTeam)
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(String.self, forKey: .id)
        type = try container.decode(ChallengeType.self, forKey: .type)
        idx = try container.decode(Int.self, forKey: .idx)
        startDate = try container.decode(Date.self, forKey: .startDate)
        endDate = try container.decode(Date.self, forKey: .endDate)
        isTeam = try container.decode(Bool.self, forKey: .isTeam)
    }
    
    init(id: String, type: ChallengeType, idx: Int, startDate: Date, endDate: Date, isTeam: Bool) {
        self.id = id
        self.type = type
        self.idx = idx
        self.startDate = startDate
        self.endDate = endDate
        self.isTeam = isTeam
    }
}
