//
//  Explorer.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import Foundation

class Explorer: ObservableObject, Codable, Identifiable {
    let id = UUID()
    
    // Base
    @Published var name: String
    @Published var expertise: Expertise
    @Published var shift: Shift
    @Published var image: String // Harusnya nanti URL
    @Published var mentor: String?
    @Published var isMentor: Bool
    
    var imageURL: String{
        API.BASE_URL + image
    }
    
    // MARK: Codable
    enum CodingKeys: CodingKey {
        case name, expertise, shift, image, mentor, isMentor
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(name, forKey: .name)
        try container.encode(expertise, forKey: .expertise)
        try container.encode(shift, forKey: .shift)
        try container.encode(image, forKey: .image)
        try container.encode(mentor, forKey: .mentor)
        try container.encode(isMentor, forKey: .isMentor)
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        name = try container.decode(String.self, forKey: .name)
        expertise = try container.decode(Expertise.self, forKey: .expertise)
        shift = try container.decode(Shift.self, forKey: .shift)
        image = try container.decode(String.self, forKey: .image)
        mentor = try container.decodeIfPresent(String.self, forKey: .mentor)
        isMentor = try container.decode(Bool.self, forKey: .isMentor)
    }
    
    init(name: String, expertise: Expertise, shift: Shift, image: String, mentor: String?, isMentor: Bool) {
        self.name = name
        self.expertise = expertise
        self.shift = shift
        self.image = image
        self.mentor = mentor
        self.isMentor = isMentor
    }
}

