//
//  Shift.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 05/05/21.
//

import Foundation
import SwiftUI

enum Shift: Int, Codable{
    case morning
    case afternoon
    case all
    
    var text: String{
        if self == .morning{
            return "Morning"
        }else if self == .afternoon{
            return "Afternoon"
        }else{
            return "Morning & Afternoon"
        }
    }
    var color: Color{
        if self == .morning{
            return Color(#colorLiteral(red: 1, green: 0.9529604316, blue: 0.5241448879, alpha: 1))
        }else if self == .afternoon{
            return Color(#colorLiteral(red: 0.7051548958, green: 0.9668112397, blue: 1, alpha: 1))
        }else{
            return Color(#colorLiteral(red: 0.5659160018, green: 0.9811808467, blue: 0.6088673472, alpha: 1))
        }
    }
}

enum Expertise: Int, Codable {
    case tech
    case design
    case domainExpert
    
    var text: String{
        if self == .tech{
            return "Tech / IT / IS"
        }else if self == .design{
            return "Design"
        }else{
            return "Domain Expert"
        }
    }
    var color: Color{
        if self == .tech{
            return Color(#colorLiteral(red: 1, green: 0.6640416384, blue: 0.6169000864, alpha: 1))
        }else if self == .design{
            return Color(#colorLiteral(red: 0.9455657601, green: 0.8105966449, blue: 0.9637162089, alpha: 1))
        }else{
            return Color(#colorLiteral(red: 0.5995026231, green: 0.9628024697, blue: 0.6899049282, alpha: 1))
        }
    }
}

enum ChallengeType: Int, Codable {
    case nanoChallenge
    case miniChallenge
    case macroChallenge
    
    var text: String{
        if self == .nanoChallenge{
            return "Nano"
        }else if self == .miniChallenge{
            return "Mini"
        }else{
            return "Macro"
        }
    }
    var color: Color{
        if self == .nanoChallenge{
            return Color(#colorLiteral(red: 0.8047397733, green: 1, blue: 0.6186357737, alpha: 1))
        }else if self == .miniChallenge{
            return Color(#colorLiteral(red: 1, green: 0.9548960328, blue: 0.6817207336, alpha: 1))
        }else{
            return Color(#colorLiteral(red: 1, green: 0.7606083751, blue: 0.8994346261, alpha: 1))
        }
    }
    var colorDark: Color{
        if self == .nanoChallenge{
            return Color(#colorLiteral(red: 0, green: 0.6100537777, blue: 0, alpha: 1))
        }else if self == .miniChallenge{
            return Color(#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1))
        }else{
            return Color(#colorLiteral(red: 1, green: 0.2361405492, blue: 0.2419204712, alpha: 1))
        }
    }
}
