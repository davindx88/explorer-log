//
//  Team.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 06/05/21.
//

import Foundation
import SwiftUI

class Team: Codable{
    // Base
    var members: [Explorer]
    var name: String
    var shift: Shift
    var idea: String
    var challengeStatement: String
    var mentor: Explorer
    var app: String
    var challenge: Challenge

    
    // MARK: Codable
    enum CodingKeys: CodingKey {
        case members,name,shift,idea,challengeStatement,mentor,app,challenge
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(members, forKey: .members)
        try container.encode(name, forKey: .name)
        try container.encode(shift, forKey: .shift)
        try container.encode(idea, forKey: .idea)
        try container.encode(challengeStatement, forKey: .challengeStatement)
        try container.encode(mentor, forKey: .mentor)
        try container.encode(app, forKey: .app)
        try container.encode(challenge, forKey: .challenge)
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        members = try container.decode([Explorer].self, forKey: .members)
        name = try container.decode(String.self, forKey: .name)
        shift = try container.decode(Shift.self, forKey: .shift)
        idea = try container.decode(String.self, forKey: .idea)
        challengeStatement = try container.decode(String.self, forKey: .challengeStatement)
        mentor = try container.decode(Explorer.self, forKey: .mentor)
        app = try container.decode(String.self, forKey: .app)
        challenge = try container.decode(Challenge.self, forKey: .challenge)
    }
    
    init(members: [Explorer], name: String, shift: Shift, idea: String, challengeStatement: String, mentor: Explorer, app: String, challenge: Challenge) {
        self.members = members
        self.name = name
        self.shift = shift
        self.idea = idea
        self.challengeStatement = challengeStatement
        self.mentor = mentor
        self.app = app
        self.challenge = challenge
    }
}
