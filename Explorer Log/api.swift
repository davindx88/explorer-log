//
//  api.swift
//  ELCIP
//
//  Created by Davin Djayadi on 13/08/22.
//

import Foundation

struct API{
    static var BASE_URL: String{
        ProcessInfo.processInfo.environment["SERVER_URL"]!
    }
    static func getAllSeniorExplorers(completion: @escaping ([Explorer])->Void){
        self.getAllExplorers(urlString: "\(API.BASE_URL)/explorers?mentor=1", completion: completion)
    }
    static func getAllJuniorExplorers(completion: @escaping ([Explorer])->Void){
        self.getAllExplorers(urlString: "\(API.BASE_URL)/explorers?mentor=0", completion: completion)
    }
    static func getAllExplorers(completion: @escaping ([Explorer])->Void){
        self.getAllExplorers(urlString: "\(API.BASE_URL)/explorers", completion: completion)
    }
    static func getExplorer(name: String, completion: @escaping (Explorer)->Void){
        let urlStr = "\(API.BASE_URL)/explorers/\(name)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard let url = URL(string: urlStr) else {
            print("Invalid URL")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            if let response = try? JSONDecoder().decode(Explorer.self, from: data) {
                completion(response)
            }else{
                print("Decode fail")
            }
        }.resume()
    }
    static func getAllTeamsByMember(memberName: String, completion: @escaping([Team])->Void){
        let urlString = "\(API.BASE_URL)/teams?member=\(memberName)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        getAllTeams(urlString: urlString, completion: completion)
    }
    static func getAllTeamsByChallengeId(challengeId: String, completion: @escaping([Team])->Void){
        getAllTeams(urlString: "\(API.BASE_URL)/teams?challengeId=\(challengeId)", completion: completion)
    }
    static func getAllTeams(completion: @escaping([Team])->Void){
        getAllTeams(urlString: "\(API.BASE_URL)/teams", completion: completion)
    }
    static func getAllChallenges(completion: @escaping ([Challenge])->Void){
        let urlStr = "\(API.BASE_URL)/challenges"
        guard let url = URL(string: urlStr) else {
            print("Invalid URL")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            if let response = try? JSONDecoder().decode([Challenge].self, from: data) {
                completion(response)
            }else{
                print("Decode Challenge fail")
            }
        }.resume()
    }
    
    // Helper Function
    private static func getAllExplorers(urlString: String, completion: @escaping([Explorer])->Void){
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            if let response = try? JSONDecoder().decode([Explorer].self, from: data) {
                completion(response)
            }else{
                print("Decode Explorers fail")
            }
        }.resume()
    }
    private static func getAllTeams(urlString: String, completion: @escaping ([Team])->Void){
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            if let response = try? JSONDecoder().decode([Team].self, from: data) {
                completion(response)
            }else{
                print("Decode Team fail")
            }
        }.resume()
    }
}
