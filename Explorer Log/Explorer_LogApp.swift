//
//  Explorer_LogApp.swift
//  Explorer Log
//
//  Created by Davin Djayadi on 04/05/21.
//

import SwiftUI

@main
struct Explorer_LogApp: App {
    var body: some Scene {
        WindowGroup {
            MainNavigation()
        }
    }
}
